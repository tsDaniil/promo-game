angular.module("game", [])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider

                .otherwise('/');

            $stateProvider

                .state("game", {
                    url: "/game",
                    views: {
                        "main": {
                            templateUrl: "js/game/game.html",
                            controller: "GameCtrl"
                        }
                    },
                    onEnter: function () {
                        if (OrientationManager.getOrientation() != "vertical") {
                            GameManager.showOrientationDialog();
                        }
                    }
                })

                .state("score", {
                    url: "/score",
                    views: {
                        "main": {
                            templateUrl: "js/game/score.html",
                            controller: "ScoreCtrl"
                        }

                    }
                })

        }]
);