angular.module('game')
    .controller('GameCtrl', ["$scope", "$state", function ($scope, $state) {

        $scope.size = document.querySelector(".logo").clientHeight;

        $scope.setHeight = function () {
            $scope.size = document.querySelector(".logo").clientHeight;
        };

        window.addEventListener("resize", function () {

            $scope.size = document.querySelector(".logo").clientHeight;
            $scope.$apply();

        });

        $scope.foodCount = ["0"];
        $scope.prizeCount = ["0"];

        $scope.seconds = ["0", "0"];
        $scope.minutes = ["0", "0"];

        $scope.setFoodCount = function (count) {

            $scope.foodCount = (count + "").split("");
            $scope.$apply();

        };

        $scope.setPrizeCount = function (count) {

            $scope.prizeCount = (count + "").split("");
            $scope.$apply();

        };

        $scope.updateTime = function (time) {

            time = time + "";
            if (time.length == 1) {
                time = "0" + time;
            }

            if (time != $scope.seconds.join("")) {

                if (time < 5) {
                    $scope.seconds = time.split("").map(function (num) {
                        return num + "r"
                    });
                    if ($scope.minutes[0].indexOf("r") == -1) {
                        $scope.minutes = $scope.minutes.map(function (num) {
                            return num + "r";
                        });
                    }
                } else {
                    $scope.seconds = time.split("");
                }
                $scope.$apply();
            }

        };

        if (OrientationManager.getOrientation() == "horizontal") {
            $scope.readed = true;
        }

        var readed = false;

        $scope.setReaded = function () {

            readed = true;

            GameManager.start();
            GameManager.game.setFuncs($scope.setPrizeCount, $scope.setFoodCount, $scope.updateTime);
            GameManager.onEnd(function () {
                $state.go("score");
                SoundManager.gameEnd.play();
            });
            $scope.readed = true;

        };

        OrientationManager.bind(OrientationManager.eventName, function (orientation) {
            if (orientation == "vertical" && !readed) {
                $scope.readed = false;
            } if (orientation == "horizontal" && !readed) {
                $scope.readed = true;
            }
            $scope.$apply();
        });

    }]
);
