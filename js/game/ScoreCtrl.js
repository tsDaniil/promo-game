angular.module('game')
    .controller('ScoreCtrl', ["$scope", "$state", function ($scope, $state) {

        var food, prizes;

        if (GameManager.game) {
            food = GameManager.game.foodCount || 0;
            prizes = GameManager.game.prizeCount || 0;
            GameManager.game = null;
        } else {
            food = 0;
            prizes = 0;
            $state.go("home");
        }

        window.addEventListener("resize", function () {

            $scope.size = document.querySelector(".logo").clientHeight;
            $scope.$apply();

        });

        $scope.imagesLoadedCount = 0;
        $scope.foodImages = (food + "").split("");
        $scope.prizeImages = (prizes + "").split("");

        setTimeout(function () {
            $scope.size = document.querySelector(".logo").clientHeight;
            $scope.$apply();
        }, 100);

        $scope.imageLoaded = function () {
            $scope.imagesLoadedCount++;
            if ($scope.imagesLoadedCount == document.querySelectorAll("img").length) {
                $scope.size = document.querySelector(".logo").clientHeight;
            }
        };

    }]
);
