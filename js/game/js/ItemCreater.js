/**
 * @class ItemCreater
 */
var ItemCreater = Base.extend({

    /**
     * @param {String[]} foodImages
     * @param {String[]} prizesImages
     * @param {String[]} tokenImages
     * @param {ImagesData} imagesData
     * @constructor
     */
    constructor: function ItemCreater(foodImages, prizesImages, tokenImages, imagesData) {

        /**
         * @type {ImagesData}
         */
        this._imagesData = imagesData;

        /**
         * type {SizeI}
         */
        this.nodeSize = null;

        /**
         * @type {Game}
         */
        this.parent = null;

        /**
         * Счетчик, который говорит надо ли класть приз в еду
         * @type {number}
         */
        this.prizrChanse = 0;

        /**
         * @type {Image[]}
         * @private
         */
        this._foodImages = null;

        /**
         * @type {Image[]}
         * @private
         */
        this._prizesImages = null;

        /**
         * @type {Image[]}
         * @private
         */
        this._tokenImages = null;

        this._loadImages(foodImages, prizesImages, tokenImages);

    },

    /**
     * Скачиваем картинки и кешируем их
     * @param {String[]} foodImages
     * @param {String[]} prizesImages
     * @param {String[]} tokenImages
     * @private
     */
    _loadImages: function (foodImages, prizesImages, tokenImages) {

        var load = function (imageSrc) {

            var that = this;

            var image = new Image();

            //image.onload = function () {
            //
            //    var canvas = document.createElement("canvas");
            //    canvas.width = that._imagesData.size.width;
            //    canvas.height = that._imagesData.size.height;
            //    canvas.className = "preloaded";
            //    document.body.appendChild(canvas);
            //    var ctx = canvas.getContext("2d");
            //    ctx.drawImage(this, 0,0, canvas.width, canvas.height);
            //
            //    if (this.src.indexOf("/food/") != -1) {
            //        that._foodImages = that._foodImages.map(function (img) {
            //            if (img.src === this.src) {
            //                console.log("replace");
            //                return canvas;
            //            } else {
            //                return img;
            //            }
            //        }, this);
            //    }
            //
            //    if (this.src.indexOf("/prizes/") != -1) {
            //        that._prizesImages = that._prizesImages.map(function (img) {
            //            if (img.src === this.src) {
            //                console.log("replace");
            //                return canvas;
            //            } else {
            //                return img;
            //            }
            //        }, this);
            //    }
            //
            //};
            image.setAttribute("data-src", imageSrc);
            image.setAttribute("data-blur", imageSrc.replace(".png", "_blured.png"));
            image.src = imageSrc;
            return image;

        }.bind(this);

        this._foodImages = foodImages.map(load);
        this._prizesImages = prizesImages.map(load);
        this._tokenImages = tokenImages.map(load);

    },

    /**
     * Устанавливаем размеры холста
     * @param {SizeI} size
     */
    setCanvasSize: function (size) {
        this.nodeSize = size;
    },

    /**
     * Устанавливаем ссылку на объект игры
     * @param {Game} parent
     */
    setParent: function (parent) {
        this.parent = parent;
    },

    /**
     * Метод создания предметов игры
     * @param callback
     * @return {Array}
     */
    create: function (callback) {

        if (!this.isNeedCreate()) {
            return [];
        }

        var imageType = ItemCreater.getType();
        var result = [];

        var itemData = this._imagesData;
        var coord = this.getCoord(itemData.size);
        var image = this.getImage(imageType);
        var prize;

        switch (imageType) {
            case "foodImages":
                this.prizrChanse++;
                if (this.prizrChanse == 3) {
                    this.prizrChanse = 0;
                    prize = this.getImage("prizesImages");
                }
                result.push(new Food(this.parent, coord, image, itemData.size, prize));
                break;
            case "tokenImages":
                result.push(new Token(this.parent, coord, image, itemData.size));
                break;
        }

        callback(result);

    },

    /**
     * Получаем картинку по типу
     * @param {String} imageType
     * @return {Image}
     */
    getImage: function (imageType) {
        var images = this.getImages(imageType);
        return images[ItemCreater.getRandomInt(0, images.length - 1)];
    },

    /**
     * Получаем массив картинок по типу
     * @param imageType
     * @return {Image[]}
     */
    getImages: function (imageType) {
        return this["_" + imageType];
    },

    /**
     * Получаем координату X
     * @param {SizeI} size
     * @return {Number}
     */
    getCoord: function (size) {
        return ItemCreater.getRandomInt(0, this.nodeSize.width - size.width);
    },

    /**
     *
     * @return {boolean}
     */
    isNeedCreate: function () {
        return ItemCreater.getRandomInt(0, 100) > 30;
    },

    static: {

        getRandomInt: function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        },

        getType: function () {
            var types = ["foodImages", "tokenImages"];
            return types[ItemCreater.getRandomInt(0, types.length - 1)];
        }

    }
});