/**
 * Created by daniil on 27.11.14.
 */
var AnimationEffect = Effect.extend({

    constructor: function AnimationEffect(coords, size, startOpacity, removedTime, needEffect) {

        this._image = GameManager.game.effectObj.boom;

        this.timer = setTimeout(function () {
            this._image = GameManager.game.effectObj.boom2;

            this.timer = setTimeout(function () {
                this._image = GameManager.game.effectObj.boom3;

                this.timer = setTimeout(function () {
                    this._image = GameManager.game.effectObj.boom4;
                }.bind(this), 60);

            }.bind(this), 60);

        }.bind(this), 60);

        this._vithoytEffect = true;

        this._startOpacity = 1;

        this._removedTime = removedTime || 1000;

        /**
         * @type {{x: Number, y: Number}}
         * @private
         */
        this._coords = coords;

        /**
         * @type {SizeI}
         * @private1
         */
        this._size = size;

        this._startTime = new Date().getTime();

        /**
         * Флаг надо ли удалять объект
         * @type {boolean}
         * @private
         */
        this._needRemove = false;

        this._waitAndRemove(this._removedTime);

    }

});