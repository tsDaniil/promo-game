/**
 * Created by daniil on 17.11.14.
 */

/**
 * @class Effect
 */
var Effect = Base.extend({

    /**
     * @param {Image} image
     * @param {Object} coords
     * @param {Number} coords.x
     * @param {Number} coords.y
     * @param {Number} [startOpacity]
     * @param {Number} [removedTime]
     * @param {Boolean} [needEffect]
     * @param {SizeI} size
     * @constructor
     */
    constructor: function Effect(image, coords, size, startOpacity, removedTime, needEffect) {

        /**
         * @type {Image}
         * @private
         */
        this._image = image;

        this._vithoytEffect = needEffect || false;

        this._startOpacity = startOpacity == 0 ? 0 : 1;

        this._removedTime = removedTime || 1000;

        /**
         * @type {{x: Number, y: Number}}
         * @private
         */
        this._coords = coords;

        /**
         * @type {SizeI}
         * @private1
         */
        this._size = size;

        this._startTime = new Date().getTime();

        /**
         * Флаг надо ли удалять объект
         * @type {boolean}
         * @private
         */
        this._needRemove = false;

        this._waitAndRemove(this._removedTime);

    },

    /**
     * Назначает таймер для удаления объекта
     * @param {number} removeTime
     * @private
     */
    _waitAndRemove: function (removeTime) {
        this.timer = setTimeout(function () {
            this._needRemove = true;
            this.removed();
        }.bind(this), removeTime);
    },

    removed: function () {

    },

    stopTimers: function () {
        clearTimeout(this.timer);
    },

    getOpacity: function () {
        return Math.abs(this._startOpacity - ((new Date().getTime() - this._startTime) / this._removedTime));
    },

    /**
     * Рисуем эффект
     * @param ctx
     */
    draw: function (ctx) {
        if (!this._vithoytEffect) {
            ctx.globalAlpha = Math.pow(this.getOpacity(), 3) * 5;
        }
        ctx.drawImage(this._image, this._coords.x, this._coords.y, this._size.width, this._size.height);
        ctx.globalAlpha = 1.0;
    },

    /**
     * Проверяем надо ли удалять
     * @return {boolean}
     */
    isNeedRemove: function () {
        return this._needRemove;
    }

});