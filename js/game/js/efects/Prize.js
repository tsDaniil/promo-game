/**
 * Created by daniil on 18.11.14.
 */

/**
 * @extends Effect
 * @class Prize
 */
var Prize = Effect.extend({

    constructor: function (image, coords, size) {

        Effect.call(this, image, coords, size);

    },

    removed: function () {

        GameManager.game.addEffect(new Effect(this._image, this._coords, this._size).setOpacitiMode());

    }

});