/**
 * @class GameManager
 */
var GameManager = Base.extend({

    /**
     * @constructor
     */
    constructor: function GameManager() {

        /**
         * Картинки с едой
         * @type {Array}
         * @private
         */
        this._foodImages = [];

        /**
         *
         * @type {null}
         */
        this.scale = null;

        /**
         * Картинки призов
         * @type {Array}
         * @private
         */
        this._prizesImages = [];

        /**
         * Картинки предметов
         * @type {Array}
         * @private
         */
        this._tokenImages = [];

        /**
         * Картинки эффектов
         * @type {Array}
         * @private
         */
        this._effectImages = [];


        /**
         * Данные о размере картинок
         * @type {ImagesData}
         * @private
         */
        this._imagesData = null;

        /**
         * Объект игры
         * @type {Game}
         */
        this.game = null;

        /**
         * Объект создающий объекты игры
         * @type {ItemCreater}
         * @private
         */
        this._itemCreater = null;

        /**
         * @type {EffectImages}
         */
        this.effects = null;

        this._setImages();
        this._setImagesData();
        this._setItemCreater();

        this._setHandlers();

        this.initOrientation();

    },

    initOrientation: function () {

        $(function () {

            if (OrientationManager.getOrientation() != "vertical") {

                this.showOrientationDialog();

            }

            OrientationManager.bind(OrientationManager.eventName, function (orientation) {

                if (orientation == "vertical") {
                    this.hideOrientationDialog();
                    if (GameManager.game) {
                        var canvas = document.createElement("canvas");
                        canvas.width = GameManager.game.nodeSize.width;
                        canvas.height = GameManager.game.nodeSize.height;
                        GameManager.game.ctx = canvas.getContext("2d");
                        document.querySelector(".game").appendChild(canvas);
                    }
                } else {
                    this.showOrientationDialog();
                    if (GameManager.game) {
                        document.querySelector("canvas").parentNode.removeChild(document.querySelector("canvas"));
                    }
                }

            }, this);

        }.bind(this));
    },


showOrientationDialog: function () {
        if (!this.dialog) {
            this.createDialog();
        }
        if (location.hash.indexOf("#/game") != -1) {
            if (this.game) {
                this.game.pause();
            }
            document.querySelector("#site").appendChild(this.dialog);
        }
    },

    hideOrientationDialog: function () {
        if (this.game) {
            this.game.play();
        }
        if (this.dialog.parentNode) {
            document.querySelector("#site").removeChild(this.dialog);
        }
    },

    createDialog: function () {

        this.dialog = document.createElement("div");
        this.dialog.className = "dialog";

        var text = document.createElement("img");
        text.className = "pause-text";
        text.src = "img/pause.png";
        this.dialog.appendChild(text);

    },

    /**
     * Назначаем обработчики
     * @private
     */
    _setHandlers: function () {

        var Manager = this;

        var handler = function (event) {

            event.stopPropagation();
            event.preventDefault();

            var tap = function (event) {
                if (!GameManager.game.collection.some(function (Item) {
                        if (Item.hitTest(event)) {
                            Item.onTap();
                            return true;
                        } else {
                            return false;
                        }
                    })) {
                }
            };


            function getCoord(event) {
                var x = event.pageX || event.clientX,
                    y = event.pageY || event.clientY;
                return {
                    x: x * (1 / Manager.scale),
                    y: y * (1 / Manager.scale)
                };
            }

            if (event.touches) {
                Array.prototype.forEach.call(event.touches, function (event) {
                    tap(getCoord.call(this, event));
                });
            } else {
                tap(getCoord.call(this, event));
            }

            return false;
        };

        var isCanvas = function (event) {
            event = event || window.event;
            if (event.target.tagName == "CANVAS") {
                handler(event);
            }
        };

        document.addEventListener("mousedown", isCanvas, false);
        document.addEventListener("touchstart", isCanvas, false);
        document.addEventListener("MSPointerDown", isCanvas, false);

    },

    /**
     * Задаем список путей до картинок
     * @private
     */
    _setImages: function () {

        this._foodImages = [

            "img/food/Cola.png",
            "img/food/BM.png",
            "img/food/Pie.png",
            "img/food/Filte.png",
            "img/food/BT.png",
            "img/food/Royal_cheese.png",
            "img/food/Fries.png",
            "img/food/coffee.png",
            "img/food/Royal_deluxe.png",
            "img/food/Ice_cream.png"

        ];

        this._prizesImages = [

            "img/prizes/Litres.png",
            "img/prizes/Tablet.png",
            "img/prizes/Airberlin.png",
            "img/prizes/Phone.png",
            "img/prizes/Tui.png",
            "img/prizes/Camera.png",
            "img/prizes/Citrooen.png",
            "img/prizes/Tv.png",
            "img/prizes/Coca-cola.png",
            "img/prizes/Pro_camera.png",
            "img/prizes/visa.png",
            "img/prizes/Headphones.png",
            "img/prizes/Radio.png",
            "img/prizes/Wii_2.png",
            "img/prizes/Lamoda.png",
            "img/prizes/Sotmarket.png",
            "img/prizes/Wii.png"

        ];

        this._tokenImages = [

            "img/tokens/Barrow.png",
            "img/tokens/Dog.png",
            "img/tokens/Iron.png",
            "img/tokens/Thimble.png",
            "img/tokens/Boot.png",
            "img/tokens/Hat.png",
            "img/tokens/Ship.png"

        ];

        this._effectImages = [

            "img/fx/200x200_miss.png",
            "img/fx/add.png",
            "img/fx/Boom_1.png",
            "img/fx/Boom_2.png",
            "img/fx/Boom_3.png",
            "img/fx/Boom_4.png"

        ];

        this.blured = {

            "img/prizes/Airberlin_blured.png": null,
            "img/prizes/Phone_blured.png": null,
            "img/prizes/Tui_blured.png": null,
            "img/prizes/Camera_blured.png": null,
            "img/prizes/Tv_blured.png": null,
            "img/prizes/Coca-cola_blured.png": null,
            "img/prizes/Pro_camera_blured.png": null,
            "img/prizes/visa_blured.png": null,
            "img/prizes/Headphones_blured.png": null,
            "img/prizes/Radio_blured.png": null,
            "img/prizes/Wii_2_blured.png": null,
            "img/prizes/Lamoda_blured.png": null,
            "img/prizes/Sotmarket_blured.png": null,
            "img/prizes/Wii_blured.png": null,
            "img/prizes/Litres_blured.png": null,

            "img/tokens/Barrow_blured.png": null,
            "img/tokens/Dog_blured.png": null,
            "img/tokens/Iron_blured.png": null,
            "img/tokens/Thimble_blured.png": null,
            "img/tokens/Boot_blured.png": null,
            "img/tokens/Hat_blured.png": null,
            "img/tokens/Ship_blured.png": null,

            "img/food/BM_blured.png": null,
            "img/food/Pie_blured.png": null,
            "img/food/Filte_blured.png": null,
            "img/food/BT_blured.png": null,
            "img/food/Royal_cheese_blured.png": null,
            "img/food/Fries_blured.png": null,
            "img/food/coffee_blured.png": null,
            "img/food/Royal_deluxe_blured.png": null,
            "img/food/Ice_cream_blured.png": null,
            "img/food/Cola_blured.png": null
        };

        var getImage = function (src) {
            var image = new Image();
            image.setAttribute("data-src", src);
            image.src = src;
            return image;
        };

        this.effects = {
            miss: getImage(this._effectImages[0]),
            add: getImage(this._effectImages[1]),
            boom: getImage(this._effectImages[2]),
            boom2: getImage(this._effectImages[3]),
            boom3: getImage(this._effectImages[4]),
            boom4: getImage(this._effectImages[5]),
            yellow: getImage(this._effectImages[5])
        };

        for (var i in this.blured) {

            if (this.blured.hasOwnProperty(i)) {
                this.blured[i] = getImage(i);
            }

        }

    },

    /**
     *  Создаем объект создания объектов игры
     * @private
     */
    _setItemCreater: function () {

        this._itemCreater = new ItemCreater(this._foodImages, this._prizesImages, this._tokenImages, this._imagesData);

    },

    /**
     * Устанавливаем данные для объектов игры
     * @private
     */
    _setImagesData: function () {

        this._imagesData = {
            size: {
                width: 97,
                height: 97
            }
        };

    },

    setScale: function (scale) {
        this.scale = scale;
    },

    /**
     * Создаем и запускаем новый экземпляр игры
     */
    start: function () {
        this.game = new Game(this._imagesData, this._itemCreater, this.effects, this);
        if (OrientationManager.getOrientation() == "vertical") {
            this.game.play();
        } else {
            this.showOrientationDialog();
        }
    },

    /**
     * Ставим игру на паузу
     */
    pause: function () {
        this.game.pause();
    },

    /**
     * Снимаем игру с паузы
     */
    play: function () {
        this.game.play();
    },

    onEnd: function (callback) {
        this.onEndCallback = callback;
    },

    stop: function () {
        if (this.game) {
            this.game.pause();
            this.game = null;
        }
    },

    getScore: function () {
        return this.game.prizeCount;
    }
});

$(function () {
    GameManager = new GameManager();
});