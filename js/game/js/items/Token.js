/**
 * Created by daniil on 17.11.14.
 */

/**
 * @class Token
 * @extends Item
 */
var Token = Item.extend({

    constructor: function Food() {

        Item.apply(this, arguments);

    },

    onTap: function () {
        vibrate(300);
        this.parent.addEffect(new Effect(this.parent.getEffects().miss, this.coords, this.size));
        GameManager.isBlured = true;
        setTimeout(function () {
            GameManager.isBlured = false;
        }, 500);
    }

});