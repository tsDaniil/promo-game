/**
 * Created by daniil on 17.11.14.
 */

/**
 *
 * @class Item
 */
var Item = Base.extend({

    /**
     *
     * @param parent
     * @param coodX
     * @param {HTMLImageElement} image
     * @param {SizeI} size
     * @param speed
     * @constructor
     */
    constructor: function Item(parent, coodX, image, size) {

        this.parent = parent;
        this.coords = {
            x: coodX,
            y: -size.height
        };

        this.size = size;

        this._image = image;

        this._image.width = size.width;
        this._image.height = size.height;

        this._needRemove = false;

    },

    isNeedRemove: function (height) {
        if (this._needRemove) {
            return true;
        } else {
            if (this.coords.y >= height) {
                this.onOut();
                return true;
            } else {
                return false;
            }
        }
    },

    hitTest: function (event) {
        return (event.x >= this.coords.x && event.y >= this.coords.y && event.x <= this.coords.x + this.size.width && event.y <= this.coords.y + this.size.height);
    },

    onOut: function () {

    },

    onTap: function () {

    },

    /**
     *
     * @param {CanvasRenderingContext2D} ctx
     * @param {number} distance
     */
    move: function (ctx, distance) {
        this.coords.y += distance;
        if (GameManager.isBlured) {
            ctx.drawImage(GameManager.blured[this._image.getAttribute("data-blur")], this.coords.x, this.coords.y, this.size.width, this.size.height);
        } else {
            ctx.drawImage(this._image, this.coords.x, this.coords.y, this.size.width, this.size.height);
        }
    },

    remove: function () {
        this._needRemove = true;
    }

});