/**
 * Created by daniil on 17.11.14.
 */

/**
 * @class Food
 * @extends Item
 */
var Food = Item.extend({

    constructor: function Food(parent, coodX, image, size, prize) {

        Item.call(this, parent, coodX, image, size);

        this.prize = prize;

    },

    onTap: function () {
        if (this.prize) {
            this.parent.addEffect(new Effect(GameManager.game.effectObj.yellow, this.coords, this.size, 1, this.prize ? null : 500, true));
            this.parent.addEffect(new Effect(this.prize, this.coords, this.size));
            this.parent.addEffect(new Effect(this.parent.effectObj.add, this.coords, this.size, 0));
            this.parent.addSecond();
            this.parent.upPrizeCount();
        } else {
            this.parent.addEffect(new AnimationEffect(this.coords, this.size, 1, this.prize ? null : 500, true));
            this.parent.upFoodsCount();
        }
        this.remove();
    }

});