/**
 * @class Game
 */
var Game = Base.extend({

    /**
     * @param {ImagesData} gameData
     * @param {ItemCreater} itemCreater
     * @param {EffectImages} effectObject
     * @param {GameManager} parent
     * @constructor
     */
    constructor: function Game(gameData, itemCreater, effectObject, parent) {

        /**
         * Ссылка на объект создателя предметов игры
         * @type {ItemCreater}
         */
        this.creater = itemCreater;

        this.prizeCount = 0;

        this.foodCount = 0;

        this.foodDom = document.querySelector(".food-count");
        this.prizeDom = document.querySelector(".prize-count");


        /**
         * @type {GameManager}
         */
        this.parent = parent;

        /**
         * Ссылка на объект данных для картинки
         * @type {ImagesData}
         */
        this.data = gameData;

        /**
         * Скорость падения предметов
         * @type {number}
         */
        this.speed = 7 / (1000 / 60);

        /**
         * Коллекция предметов в данной игре
         * @type {Array}
         */
        this.collection = [];

        /**
         * Объект с картинками эффектов
         * @type {EffectImages}
         */
        this.effectObj = effectObject;

        /**
         * Коллекция активных эффектов
         * @type {Array}
         */
        this.effects = [];

        /**
         * Холст для отрисовки игры
         * @type {HTMLCanvasElement}
         */
        this.canvas = document.querySelector("canvas");

        /**
         * Контекст отрисовки холста
         * @type {CanvasRenderingContext2D}
         */
        this.ctx = this.canvas.getContext("2d");

        this.startTime = new Date().getTime();

        this.gameTime = 30000;

        this.setCanvasAttrs();

        /**
         * Размеры холста
         * @type SizeI
         */
        this.nodeSize = {
            width: this.canvas.width,
            height: this.canvas.height
        };

        itemCreater.setParent(this);
        itemCreater.setCanvasSize(this.nodeSize);

    },

    setCanvasAttrs: function () {

        this.canvas.width = $(this.canvas).parent().width();
        this.canvas.height = $(this.canvas).parent().height();

        /**
         * Размеры холста
         * @type SizeI
         */
        this.nodeSize = {
            width: innerWidth,
            height: innerHeight
        };

        this.parent.setScale(1);
    },

    setFuncs: function (prizeCallback, foodCallback, timeUpdateCallback) {
        this.prizeCallback = prizeCallback;
        this.foodCallback = foodCallback;
        this.timeUpdateCallback = timeUpdateCallback;
    },

    upFoodsCount: function () {
        this.foodCount++;
        this.foodCallback(this.foodCount);
    },

    upPrizeCount: function () {
        this.prizeCount++;
        this.prizeCallback(this.prizeCount);
    },

    /**
     * Получаем объект картинок эффектов
     * @return {EffectImages}
     */
    getEffects: function () {
        return this.effectObj;
    },

    /**
     * Добавляем эффект в массив эффектов
     * @param {Effect} effect
     */
    addEffect: function (effect) {
        this.effects.push(effect);
    },

    addSecond: function () {
        this.gameTime += 1000;
    },

    getTime: function () {
        return ((this.gameTime / 1000) - Math.round((new Date().getTime() - this.startTime) / 1000));
    },

    updateTime: function () {

        var time = this.getTime();
        this.timeUpdateCallback(time);

        if (time <= 0) {
            this.pause();
            this.parent.onEndCallback();
        }

    },

    /**
     * Запускаем перерисовку холста через requestAnimationFrame
     */
    play: function () {
        var that = this;
        var needCreate = true;
        var distance = 0;
        var localDistance;
        var lastTime = new Date().getTime();

        this.isStop = false;

        if (this.pauseTime) {
            this.startTime = new Date().getTime();
            this.gameTime = this.pauseTime * 1000;
            this.pauseTime = null;
        }

        var step = function () {

            that.updateTime();
            that.clear();
            var _date = new Date().getTime();
            var time = _date - lastTime;
            lastTime = _date;
            localDistance = time * that.speed;
            if (needCreate) {
                that.creater.create(function (items) {
                    if (items.length) {
                        that.collection = that.collection.concat(items);
                    }
                });
                needCreate = false;
            } else {
                distance += localDistance;
                if (distance > that.data.size.height) {
                    distance = 0;
                    needCreate = true;
                }
            }

            that.draw(localDistance);

            if (!that.isStop) {
                requestAnimationFrame(step);
            }
        };

        requestAnimationFrame(step);
    },

    /**
     * Ставим игру на паузу
     */
    pause: function () {
        this.pauseTime = this.getTime();
        this.isStop = true;
        this.stopTimers();
    },

    stopTimers: function () {
        this.effects.forEach(function (effect) {
            effect.stopTimers();
        })
    },

    /**
     * Очищаем холст
     */
    clear: function () {
        this.ctx.clearRect(0, 0, this.nodeSize.width, this.nodeSize.height);
    },

    /**
     * Отрисовываем смещенные элементы игры
     * @param {Number} distance
     */
    draw: function (distance) {
        this.collection = this.collection.filter(function (Item) {
            Item.move(this.ctx, distance);
            return !Item.isNeedRemove(this.nodeSize.height);
        }, this);
        this.effects = this.effects.filter(function (Effect) {
            Effect.draw(this.ctx);
            return !Effect.isNeedRemove();
        }, this);
    }


});