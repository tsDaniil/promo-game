angular.module("app", ["vendor", "home", "rules", "game"])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider

                .otherwise('/');

            $stateProvider

                .state("home", {
                    url: "/",
                    views: {
                        "main": {
                            templateUrl: "js/home/home.html",
                            controller: "homeCtrl"
                        }

                    }
                })

        }]
    );