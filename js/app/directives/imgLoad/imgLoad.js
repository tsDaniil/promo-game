angular.module("app")
    .directive('imgLoad', function () {
        return {
            scope: {
                ngLoad: "&"
            },
            link: ["$scope","$element", function($scope, element) {

                element.bind("load" , function(e){
                    $scope.$apply(function () {
                        $scope.ngLoad();
                    });
                });
            }]
        }
    });