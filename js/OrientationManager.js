/**
 * Created by daniil on 23.11.14.
 */

/**
 * @class OrientationManager
 * @extends Base
 */
var OrientationManager = new (Base.extend({

    /**
     * @constructor
     */
    constructor: function OrientationManager() {

        this.orientation = "";
        this.eventName = "OrientationManager:OrientationChange";

        this._setHandlers();
        this._initOrientation();

    },

    _setHandlers: function () {

        var checkOrientation = function () {


            var orientattion = this.getOrientation();
            this._initOrientation();

            if (orientattion != this.getOrientation()) {
                this.trigger(this.eventName, [this.getOrientation()]);
            }

        }.bind(this);

        window.addEventListener("orientationchange", checkOrientation, false);
        window.addEventListener("resize", checkOrientation, false);

    },

    _initOrientation: function () {
        this.orientation = innerHeight > innerWidth ? "vertical" : "horizontal";
    },

    getOrientation: function () {
        return this.orientation;
    }

}));