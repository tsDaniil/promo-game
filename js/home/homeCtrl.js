angular.module('home')
    .controller('homeCtrl', ["$scope", function ($scope) {

        $scope.size = document.querySelector(".logo").clientHeight;

        $scope.setHeight = function () {
            $scope.size = document.querySelector(".logo").clientHeight;
        };

        window.addEventListener("resize", function () {

            $scope.size = document.querySelector(".logo").clientHeight;
            $scope.$apply();

        });

    }]
);
