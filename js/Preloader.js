/**
 * Created by daniil on 27.11.14.
 */
var Preloader = Base.extend({

    constructor: function Preloader() {

        this.dom = document.querySelector(".preloader");

        this.setListToLoad();
        this.load(this.hide.bind(this));

    },

    setListToLoad: function () {

        this.images = [

            "img/Logo_block.jpg",
            "img/back.png",
            "img/money_man.png",
            "img/play.png",
            "img/How_play.png",
            "img/legal.png",
            "img/stady/txt.png",
            "img/stady/play.png",
            "img/0.png",
            "img/1.png",
            "img/2.png",
            "img/3.png",
            "img/4.png",
            "img/5.png",
            "img/6.png",
            "img/7.png",
            "img/8.png",
            "img/9.png",
            "img/0r.png",
            "img/1r.png",
            "img/2r.png",
            "img/3r.png",
            "img/4r.png",
            "img/5r.png",
            "img/6r.png",
            "img/7r.png",
            "img/8r.png",
            "img/9r.png",
            "img/result.png",
            "img/line.png",
            "img/catched_products.png",
            "img/product_prizes.png",
            "img/replay.png",
            "img/afterGameRule.png",
            "img/share.png",
            "img/social.png",
            "img/open.png",
            "img/txt_open.png",
            "img/header_horizontal.jpg",
            "img/next_slider.png",
            "img/prev_slider.png",
            "img/collect.png",
            "img/txt_block.png",
            "img/stikers.png",
            "img/prizes.png",
            "img/prizes_title.png",
            "img/prize-group.png",
            "img/prize-line.png",
            "img/button.png",
            "img/rules2.png",

            "img/food/Cola.png",
            "img/food/BM.png",
            "img/food/Pie.png",
            "img/food/Filte.png",
            "img/food/BT.png",
            "img/food/Royal_cheese.png",
            "img/food/Fries.png",
            "img/food/coffee.png",
            "img/food/Royal_deluxe.png",
            "img/food/Ice_cream.png",

            "img/prizes/Litres.png",
            "img/prizes/Tablet.png",
            "img/prizes/Airberlin.png",
            "img/prizes/Phone.png",
            "img/prizes/Tui.png",
            "img/prizes/Camera.png",
            "img/prizes/Citrooen.png",
            "img/prizes/Tv.png",
            "img/prizes/Coca-cola.png",
            "img/prizes/Pro_camera.png",
            "img/prizes/visa.png",
            "img/prizes/Headphones.png",
            "img/prizes/Radio.png",
            "img/prizes/Wii_2.png",
            "img/prizes/Lamoda.png",
            "img/prizes/Sotmarket.png",
            "img/prizes/Wii.png",

            "img/tokens/Barrow.png",
            "img/tokens/Dog.png",
            "img/tokens/Iron.png",
            "img/tokens/Thimble.png",
            "img/tokens/Boot.png",
            "img/tokens/Hat.png",
            "img/tokens/Ship.png",

            "img/fx/200x200_miss.png",
            "img/fx/add.png",
            "img/fx/Boom_1.png",
            "img/fx/Boom_2.png",
            "img/fx/Boom_3.png",
            "img/fx/Boom_4.png",

            "img/prizes/Airberlin_blured.png",
            "img/prizes/Phone_blured.png",
            "img/prizes/Tui_blured.png",
            "img/prizes/Camera_blured.png",
            "img/prizes/Tv_blured.png",
            "img/prizes/Coca-cola_blured.png",
            "img/prizes/Pro_camera_blured.png",
            "img/prizes/visa_blured.png",
            "img/prizes/Headphones_blured.png",
            "img/prizes/Radio_blured.png",
            "img/prizes/Wii_2_blured.png",
            "img/prizes/Lamoda_blured.png",
            "img/prizes/Sotmarket_blured.png",
            "img/prizes/Wii_blured.png",
            "img/prizes/Litres_blured.png",
            "img/prizes/Tablet_blured.png",

            "img/tokens/Barrow_blured.png",
            "img/tokens/Dog_blured.png",
            "img/tokens/Iron_blured.png",
            "img/tokens/Thimble_blured.png",
            "img/tokens/Boot_blured.png",
            "img/tokens/Hat_blured.png",
            "img/tokens/Ship_blured.png",

            "img/food/Pie_blured.png",
            "img/food/Filte_blured.png",
            "img/food/BT_blured.png",
            "img/food/Royal_cheese_blured.png",
            "img/food/Fries_blured.png",
            "img/food/coffee_blured.png",
            "img/food/Royal_deluxe_blured.png",
            "img/food/Ice_cream_blured.png",
            "img/food/Cola_blured.png",

            "img/slider_images/Big_tasty.png",
            "img/slider_images/BM.png",
            "img/slider_images/Cold-drink.png",
            "img/slider_images/Filet-o-Fish.png",
            "img/slider_images/fries.png",
            "img/slider_images/Hot_drink.png",
            "img/slider_images/Naggets.png",
            "img/slider_images/Royal-cheese.png",
            "img/slider_images/Royal-De-Luxe.png",
            "img/slider_images/texts/Big_tasty.png",
            "img/slider_images/texts/BM.png",
            "img/slider_images/texts/Cold-drink.png",
            "img/slider_images/texts/Filet-o-Fish.png",
            "img/slider_images/texts/fries.png",
            "img/slider_images/texts/Hot_drink.png",
            "img/slider_images/texts/Naggets.png",
            "img/slider_images/texts/Royal-cheese.png",
            "img/slider_images/texts/Royal-De-Luxe.png"

        ];

    },

    load: function (callback) {

        var percent = 0;
        var step = 100 / (this.images.length - 1);
        var that = this;

        this.setPercent(0);

        this.images.forEach(function (src) {

            var image = new Image();

            image.onload = function () {
                percent +=step;
                that.setPercent(percent);
                this.images.pop();
                if (!this.images.length) {
                    this.trigger("loaded", []);
                    document.querySelector("#site").style.opacity = "1";
                    requestAnimationFrame(callback);
                }
            }.bind(this);

            image.onerror = function () {
                alert("error! " + src);
            };

            image.src = src;

        }, this);

    },

    setPercent: function (percent) {
        document.querySelector(".progress-bar").style["width"] = percent + "%";
    },

    show: function () {
        this.dom.style.display = "block";
    },

    hide: function () {
        this.dom.style.display = "none";
    }

});

$(function () {
    Preloader = new Preloader();
});