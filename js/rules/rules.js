angular.module("rules", [])
    .config([
        '$stateProvider',
        '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider

                .otherwise('/');

            $stateProvider

                .state("rules", {
                    url: "/rules",
                    views: {
                        "main": {
                            templateUrl: "js/rules/rules.html",
                            controller: "RulesCtrl"
                        }

                    }
                })

                .state("fullRules", {
                    url: "/full-rules",
                    views: {
                        "main": {
                            templateUrl: "js/rules/full-rules.html",
                            controller: "RulesCtrl"
                        }

                    }
                });

        }]
);