angular.module('rules')
    .controller('RulesCtrl', ["$scope", function ($scope) {

        $scope.path = "img/slider_images/";

        $scope.images = [
            "Big_tasty.png",
            "BM.png",
            "Cold-drink.png",
            "Filet-o-Fish.png",
            "fries.png",
            "Hot_drink.png",
            "Naggets.png",
            "Royal-cheese.png",
            "Royal-De-Luxe.png"
        ];

        document.body.scrollTop = 0;

        $scope.active = 0;

        $scope.size = document.querySelector(".logo").clientHeight;

        $scope.setHeight = function () {
            $scope.size = document.querySelector(".logo").clientHeight;
        };

        $scope.setSliderHeight = function () {
            setTimeout(function () {
                $scope.sliderHeight = $(".slider-body").find("img").first().height() * 1.7;
                $scope.$apply()
            }, 300);
        };

        $scope.setSize = function () {
            $scope.sliderHeight = $(".slider-body").find("img").first().height() * 1.7;
            $scope.$apply();
        };

        OrientationManager.bind(OrientationManager.eventName, function () {
            $scope.setSize();
        });

        if ($(".slider-body").length) {

            function checkSlider() {
                if ($(".slider-body").find("img").length) {
                    requestAnimationFrame(function set() {
                        $scope.sliderHeight = $(".slider-body").find("img").first().height() * 1.7;
                        $scope.width = document.querySelector(".slider-body").parentNode.parentNode.clientWidth;
                        $scope.headerHeight = currentHeight();
                        $scope.$apply();
                        Preloader.bind("loaded", set);
                    });
                } else {
                    setTimeout(checkSlider, 50);
                }
            }

            checkSlider();

        }

        window.addEventListener("resize", function () {

            $scope.size = document.querySelector(".logo").clientHeight;
            $scope.$apply();

        });

        $scope.showLine = function () {

            $(".button").slideUp();
            $(".prize-line").slideDown();

        };

        $scope.move = function (direction) {

            if (direction) {
                if ($scope.active <= $scope.images.length - 2) {
                    $(".slider-body").find("img").animate({
                        "left": "-=100%"
                    });
                    $scope.active++;
                }
            } else {
                if ($scope.active >= 1) {
                    $(".slider-body").find("img").animate({
                        "left": "+=100%"
                    });
                    $scope.active--;
                }
            }

            if ($scope.active == 0) {
                $(".left.arrow").fadeOut();
            } else {
                $(".left.arrow").fadeIn();
            }

            if ($scope.active == $scope.images.length - 2) {
                $(".right.arrow").fadeOut();
            } else {
                $(".right.arrow").fadeIn();
            }

        };

        function currentHeight() {

            var factor = innerWidth / 637;
            if (factor >= 1) {
                return 389;
            } else {
                return 389 * factor;
            }

        }

        window.addEventListener("resize", function check() {

            if (document.querySelector(".slider-body")) {
                $scope.width = document.querySelector(".slider-body").parentNode.parentNode.clientWidth;
                $scope.headerHeight = currentHeight();
                $scope.$apply();
            } else {
                window.removeEventListener("resize", check);
            }

        }, false);

    }]
);

