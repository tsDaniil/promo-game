/**
 * @class Base
 */
var Base = new Class({

    /**
     * Содержит асоциативный массив событий с обработчиками
     * @private
     * @abstract
     */
    _events: null,

    /**
     * Запускаем событие
     *
     * @method trigger
     * @param {String} eventName     имя события
     * @param {*|Array} [arg]        аргументы которые будут переданы в событие (если аргумет 1 можно не массив)
     * @returns @this
     */
    trigger: function (eventName, arg) {

        if (!this._events || !this._events[eventName]) {
            return this;
        }

        /**
         * Проверяем массив ли аргументы и есть ли там событие
         */
        if (!(arg instanceof Array)) {
            arg = [arg];
        }

        /**
         * Запускает события конкретного узла дерева
         * @type {function(this:Base)}
         */
        var iteration = function () {

            var arr$Events = this._events[eventName].slice();

            arr$Events.some(function (handlerObj) {
                handlerObj.handler.apply(handlerObj.context, arg);
            });

        }.bind(this);

        iteration();

        return this;
    },

    /**
     * Метод назначает обработчики
     *
     * @method bind
     * @param {String|Object} eventName  Имя события или асоциативный массив событий
     * @param {function|Array} [handler] Обработчик события
     * @param {Object} [context]         Контекст вызова объекта, по дефолту this
     * @returns @this
     */
    bind: function (eventName, handler, context) {

        if (eventName == undefined) {
            console.log("Неверные параметры!");
            return this;
        }

        if (eventName instanceof Object) {
            for (var key in eventName) {
                if (eventName.hasOwnProperty(key))
                    this.bind(key, eventName[key]);
            }
            return this;
        }

        if (handler == undefined) {
            console.log("Неверные параметры!");
            return this;
        }

        if (!this._events) {
            this._events = {};
        }

        if (!this._events[eventName]) {
            this._events[eventName] = [];
        }

        this._events[eventName].push({handler: handler, context: context || this});

        return this;
    },

    /**
     * Метод отписки от события
     *
     * @method unbind
     * @param {String|Object} [eventName] Имя события
     * @param {function|String} [handler] Обработчик события | "last|first"
     * @returns @this
     */
    unbind: function (eventName, handler) {

        if (eventName == undefined) {
            this._events = {};
            return this;
        } else if (eventName instanceof Object) {
            for (var key in eventName)
                if (eventName.hasOwnProperty(key))
                    this.unbind(key, key[eventName]);

            return this;
        }

        if (handler == undefined) {
            if (this._events && eventName in this._events) {
                this._events[eventName] = [];
                return this;
            }
            return this;
        }

        if (typeof handler == "string") {
            if (handler == "last") {
                this._events[eventName].pop();
                return this;
            } else {
                this._events[eventName].shift();
                return this;
            }
        }

        for (var i = 0; i < this._events[eventName].length; i++) {

            if (this._events[eventName][i].handler == handler) {
                this._events[eventName].splice(i, 1);
                break;
            }

        }

        return this;
    }

});