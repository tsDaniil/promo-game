/**
 * Created by daniil on 17.11.14.
 */
/**
 * @class SizeI
 */
var size = {
    width: 1,
    height: 1
};

/**
 *
 * @class ImagesData
 */
var imagesData = {
    size: {
        width: 70,
            height: 70
    }
};

/**
 * @class EffectImages
 */
var effecObj = {
    miss: new Image(),
    gray: new Image(),
    white: new Image(),
    yellow: new Image()
};
