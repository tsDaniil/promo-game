/**
 * Created by daniil on 19.11.14.
 */
var css = (function () {

    var prefix = "";

    var stylesToPrefix = {

        transform: true

    };

    var prefixes = [
        "-moz-",
        "-ms-",
        "-webkit-",
        "-o-"
    ];

    function camelCase(input) {
        return input.toLowerCase().replace(/-(.)/g, function (match, group1) {
            return group1.toUpperCase();
        });
    }

    return function (element, styleName, value) {

        if (styleName in stylesToPrefix) {
            if (prefix) {
                element.style[camelCase(prefix + styleName)] = value;
            } else {
                prefixes.some(function ($prefix) {
                    if (camelCase($prefix + styleName) in element.style) {
                        prefix = $prefix;
                        return true;
                    }
                });
                element.style[camelCase(prefix + styleName)] = value;
            }
        }

        element.style[camelCase(styleName)] = value;

    }

})();

/**
 * Отлавливаем ошибки
 * @param {String} errorMsg
 * @param {String} url
 * @param {String} lineNumber
 */
window.onerror = function (errorMsg, url, lineNumber) {
    console.log('------------------\nError: ' + errorMsg + '\nScript: ' + url + '\nLine: ' + lineNumber + "\n------------------");
};

if (!window.requestAnimationFrame) {

    window.requestAnimationFrame = (function () {

        return window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {

                window.setTimeout(callback, 1000 / 60);

            };

    })();

}

function vibrate(val){
    if("vibrate" in navigator)  return navigator.vibrate(val);
    if("oVibrate" in navigator)  return navigator.oVibrate(val);
    if("mozVibrate" in navigator)  return navigator.mozVibrate(val);
    if("webkitVibrate" in navigator)  return navigator.webkitVibrate(val);
    return function () {};
}

$(document).on("click", ".rules .accordion .btn", function () {

    $(this).closest(".accordion").find(".block").slideToggle();

});