
var gulp = require("gulp"),
    fs = require("fs"),
    uglify = require("gulp-uglify"),
    minifyCSS = require('gulp-minify-css'),
    minifyHTML = require('gulp-minify-html');

gulp.task("default", ["js", "css", "html"]);

gulp.task("js", function () {

    var js = ["base/*.js", "js/**/*.js"];
    var out = ["build/base", "build/js"];

    js.forEach(function (path, index) {
        gulp.src(path)
            .pipe(uglify())
            .pipe(gulp.dest(out[index]));
    });

});

gulp.task("css", function () {

    var css = ["css/*.css", "js/**/*.css"];
    var out = ["build/css", "build/js"];

    css.forEach(function (path, index) {
        gulp.src(path)
            .pipe(minifyCSS())
            .pipe(gulp.dest(out[index]));
    });

});

gulp.task("html", function () {

    var html = ["index.html", "js/**/*.html"];
    var out = ["build", "build/js"];

    html.forEach(function (path, index) {
        gulp.src(path)
            .pipe(minifyHTML())
            .pipe(gulp.dest(out[index]));
    });

});